RESULT_MSG=`ansible-playbook -i staging /tmp/test.yml ${CHECK_MODE}`
RESULT_CODE=$?
EXPECT_MSG2=`echo "$EXPECT_MSG"|tr -d ' '`
RESULT_MSG2=`echo "$RESULT_MSG"|grep ^staging_host|tr -d ' '`
if [ "${EXPECT_CODE}" != "${RESULT_CODE}" ]; then
  echo "NG"
  echo "$RESULT_CODE"
  exit 
fi
if [ "${RESULT_MSG2}" != "${EXPECT_MSG2}" ]; then
  echo "NG"
  echo "$RESULT_MSG"
  exit 
fi
echo "OK"
