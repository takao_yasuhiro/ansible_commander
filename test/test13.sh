echo "#########################################"
echo "`basename $0`"
rm -f /tmp/test.yml
cat <<EOT > /tmp/test.yml
---
- hosts: all
  user: root
  tasks:
  - name: test
    commander:
    args:
      command: |
         echo "hogehoge" > /tmp/hoge
      test_command: "cat /tmp/hoge"
      test_command_msg: "hogehoge"
EOT
rm -r /tmp/hoge >/dev/null 2>&1
echo "#########################################"
export EXPECT_CODE=0
export EXPECT_MSG="staging_host    : ok=2    changed=1    unreachable=0    failed=0"
export CHECK_MODE="--check"
./_sub.sh

export EXPECT_CODE=0
export EXPECT_MSG="staging_host    : ok=2    changed=1    unreachable=0    failed=0"
export CHECK_MODE=""
./_sub.sh
