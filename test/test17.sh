echo "#########################################"
echo "`basename $0`"
rm -f /tmp/test.yml
cat <<EOT > /tmp/test.yml
---
- hosts: all
  user: root
  tasks:
  - name: test
    commander:
    args:
      command: |
         date> /tmp/hoge
      test_command: "test -e /tmp/hoge"
      test_command_rc: "0"
      chdir: "/tmphogehoge"
EOT
rm -r /tmp/hoge >/dev/null 2>&1
echo "#########################################"
export EXPECT_CODE=2
export EXPECT_MSG="staging_host    : ok=1    changed=0    unreachable=0    failed=1"
export CHECK_MODE="--check"
./_sub.sh

export EXPECT_CODE=2
export EXPECT_MSG="staging_host    : ok=1    changed=0    unreachable=0    failed=1"
export CHECK_MODE=""
./_sub.sh
