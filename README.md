# ansibleのコマンドの冪等性維持を簡単にするためのモジュール
ansible2にも対応しました。

ansibleは色々なモジュールがあり便利ですね。
クライアントインストールレスが嬉しいです。
構成管理以外にもリリース作業の自動化や、情報収集や調査タスクにも使えますね。今後ansibleを中心に進めていこうと思っているのですが。

しかし、複数行のコマンドを実行した際の冪等性の考慮が難しいです。
参考：http://lab.tricorn.co.jp/kamo/4836


```
- name: check default conf
  stat: path=/etc/nginx/conf.d/default.conf
  register: is_exists
- name: move default conf
  shell: mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.backup
  when: is_exists.stat.md5 is defined
```

やってられなくはないですが、
 * YAML(playbook)の行数が増えて、可読性が落ちる。
 * register等のプログラミング的な要素が出てきて書きにくい。

#そこでモジュールを作りました。

コマンド実行の冪等制のために開発したモジュール
ansible_commanderを作成した。
名前 command モジュールとshellモジュールの進化版なので、command + er を命名

# 特徴
以下の特徴を持ちます。
* 事前、事後確認のコマンドを指定できます。
* DRY RUNにも対応しています。
* コマンドを複数行にわたって書けます。
* 期待値をコマンドの返り値、もしくは標準出力、標準エラーで指定できます。


#配布場所

git clone https://bitbucket.org/takao_yasuhiro/ansible_commander.git


#インストール

下記に配置(PLAYBOOKがおいてあるディレクトリから見て)

* ansible 1系の場合: ansible1/ -> library/commander.py

* ansible 2系の場合: ansible2/ -> library/commander.py
	 
下記を参考にください。

「Ansibleを使い出す前に押さえておきたかったディレクトリ構成のベストプラクティス」
http://sechiro.hatenablog.com/entry/2015/01/06/Ansible%E3%82%92%E4%BD%BF%E3%81%84%E5%87%BA%E3%81%99%E5%89%8D%E3%81%AB%E6%8A%BC%E3%81%95%E3%81%88%E3%81%A6%E3%81%8A%E3%81%8D%E3%81%9F%E3%81%8B%E3%81%A3%E3%81%9F%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF



#使い方

  * command:必須、実行するコマンド。**複数行呼び出し可能。**　"-C, --check"オプション付き（いわゆるDRY RUN）の際には実行されません。
  * **test_command：(任意) commandを実行前後でチェックするためのコマンド群** "-C, --check"オプション付き（いわゆるDRY RUN）の際にも実行されます。
  * **test_command_rc：(任意)デフォルト 0、test_commandの実行のリターン値の期待値。**
  * test_command_msg：(任意)test_commandの標準エラーと標準出力の期待値。
  * chdir:(任意)コマンドを実行する前にcdコマンドでディレクトリを移動する。
  * creates:(任意)指定したファイル名が既に存在する場合、コマンドを実行しない。
  * executable:(任意)実行するシェルを絶対パスで指定する。
  * removes:(任意)指定したファイル名が存在しない場合、コマンドを実行しない。
  * warn:(任意)『no』または『false』を指定した場合、コマンド警告を出さない。

#実行サンプル
	  - name: 3.4 mysql_secure_installation
		commander:
		args:
		  command: |
			expect -c "
			set timeout 5
			spawn /usr/bin/mysql_secure_installation
			expect \"Enter current password for root \"
			send \"\n\"
			expect \"root password\"
			send \"Y\n\"
			expect \"New password:\"
			send \"{{ root_password }}\n\"
			expect \"Re\-enter new password\:\"
			send \"{{ root_password }}\n\"
			expect \"Remove anonymous users?\"
			send \"Y\n\"
			expect \"Disallow root login remotely?\"
			send \"Y\n\"
			expect \"Remove test database and access to it?\"
			send \"Y\n\"
			expect \"Reload privilege tables now\"
			send \"Y\n\"
			interact
			"
		  test_command: "echo exit|mysql -u root -p{{root_password}}"
	


##返り値を判別方式

	  - name: sample repo
		commander:
		args:
		  command: |
			cd /mnt/iscsi/svn
			svnadmin create sample
			chown -R apache:apache sample
		  test_command: |
			test -e /mnt/iscsi/svn/sample/
	      test_command_rc: "0"

1. test_commandを実行
2. test_commandの実行結果が
	* test_command_rcと等しい場合は３へ
	* test_command_rcと等しくない場合はSKIP
3. commandを実行
4. 3が実行が正常完了したら、test_commandを再度実行。
5. 4の実行結果がtest_commandの実行結果が
	* test_command_rcと等しくない場合はエラーとして挙げる。
	* test_command_rcと等しい場合は正常終了

##標準出力、標準エラー文字列を判別方式

	  - name:  bundle install
		commander:
		args:
		  command: |
			cd /var/lib/redmine
			source /root/.bash_profile
			bundle install
		  test_command: |
			cd /var/lib/redmine
			bundle check
		  test_command_msg: "dependencies are satisfied"

1. test_commandを実行
2. test_commandの標準出力、標準エラーが
	* test_command_msgと等しい場合は３へ
	* test_command_msgと等しくない場合はSKIP
3. commandを実行
4. 3が実行が正常完了したら、test_commandを再度実行。
5. 4のtest_commandの標準出力、標準エラーが
	* test_command_rcと等しくない場合はエラーとして挙げる。
	* test_command_rcと等しい場合は正常終了

# 所感
 従来の手順書の記載をそのままplaybookに記載すればよく
 わかりやすくなったのではと思っています。
 

